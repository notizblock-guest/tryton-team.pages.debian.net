.. include:: substitutions.txt

.. _Tryton Debian Apt Repository:

============================
Tryton Debian Apt Repository
============================

Why debian.tryton.org?
======================

Due to the different release schemata and policies of Tryton and Debian it is
currently impossible to make all Tryton releases available for the different
Debian suites in the main repository of Debian.

Out intention is to make available Tryton releases including their bug fix releases
via this aptable package mirror on debian.tryton.org. 

Using this mirror

* you can run the Tryton release of your choice on your current stable or testing Debian system (oldstable supported as long as possible).
* you are able to upgrade to the next Tryton series as soon as `you` wish to do so.
* you are independent from the next Tryton major entering in Debian main.
* you get all bug fix releases for all supported Tryton series.

.. note: Those packages are maintained by the `Debian Tryton Maintainers`_, but nevertheless they
        are unofficial and you are using them at your own risk (as always).

.. _Debian Tryton Maintainers: https://qa.debian.org/developer.php?login=maintainers%40debian.tryton.org

Scope
=====

The scope provided by Debian Tryton Maintainers is

* Released series of the `Tryton Project`_.
* Some additional dependencies needed by the Tryton packages.
* Released series of the `GNU Health Project`_.

.. _Tryton Project: http://www.tryton.org/
.. _GNU Health Project: http://health.gnu.org/

How To
======

To use this package mirror with the apt package mangement of your Debian system you must 
make available the according sources.list to your system. Please read below for further instructions
on how to do that.

To tell your system to take Tryton related packages preferrably from this mirror you should pin
them. You will also find instructions below on how to do that.

For the installation of GNU Health you find additional instructions at :doc:`gnuhealth`.

For the server configuration please refer to /usr/share/doc/tryton-server/README.Debian on your 
Debian system or look at the `latest version of README.Debian in the VCS`_.

.. _latest version of README.Debian in the VCS: https://alioth.debian.org/plugins/scmgit/cgi-bin/gitweb.cgi?p=tryton/tryton-server.git;a=blob_plain;f=debian/tryton-server.README.Debian;hb=HEAD

Distributions
=============

Naming convention for distributions is <Debian-Release>-<Tryton-Version>

e.g. jessie-3.4, stretch-3.6, buster-4-6

Packages suitable for testing and unstable are available under the distribution <Debian-Future-Release>-<Tryton-Version>

e.g. at the time of writing buster-5.0

The available distributions can be found at `Debian Tryton Distributions`_.

.. _Debian Tryton Distributions: http://debian.tryton.org/debian/dists/


Sources
=======

Adding the signature key of the mirror to apt

::

 $ sudo curl -o /etc/apt/trusted.gpg.d/debian.tryton.org-archive.gpg http://debian.tryton.org/debian/debian.tryton.org-archive.gpg


Adding the sources to apt basically works like

::

 $ sudo echo "deb http://debian.tryton.org/debian/ <distribution> main" >> /etc/apt/sources.list


Ready-to-use
------------

The following list are ready-to-use source list files. They can be downloaded and saved directly to /etc/apt/sources.list.d with

*Distribution jessie-3.2*

::

 $ sudo cd /etc/apt/sources.list.d && wget http://debian.tryton.org/debian/tryton-jessie-3.2.list

*Distribution jessie-3.4*

::

 $ FILE=tryton-jessie-3.4.list; sudo curl -o /etc/apt/sources.list.d/$FILE http://debian.tryton.org/debian/$FILE

*Distribution jessie-3.6*

::

 $ FILE=tryton-jessie-3.6.list; sudo curl -o /etc/apt/sources.list.d/$FILE http://debian.tryton.org/debian/$FILE

*Distribution jessie-3.8*

::

 $ FILE=tryton-jessie-3.8.list; sudo curl -o /etc/apt/sources.list.d/$FILE http://debian.tryton.org/debian/$FILE

*Distribution jessie-4.0*

::

 $ FILE=tryton-jessie-4.0.list; sudo curl -o /etc/apt/sources.list.d/$FILE http://debian.tryton.org/debian/$FILE

*Distribution jessie-4.2*

::

 $ FILE=tryton-jessie-4.2.list; sudo curl -o /etc/apt/sources.list.d/$FILE http://debian.tryton.org/debian/$FILE

*Distribution jessie-4.4*

::

 $ FILE=tryton-jessie-4.4.list; sudo curl -o /etc/apt/sources.list.d/$FILE http://debian.tryton.org/debian/$FILE

*Distribution jessie-4.6*

::

 $ FILE=tryton-jessie-4.6.list; sudo curl -o /etc/apt/sources.list.d/$FILE http://debian.tryton.org/debian/$FILE

 *Distribution jessie-5.0*

::

 $ FILE=tryton-jessie-5.0.list; sudo curl -o /etc/apt/sources.list.d/$FILE http://debian.tryton.org/debian/$FILE



*Distribution stretch-3.2*

::

 $ FILE=tryton-stretch-3.2.list; sudo curl -o /etc/apt/sources.list.d/$FILE http://debian.tryton.org/debian/$FILE

*Distribution stretch-3.4*

::

 $ FILE=tryton-stretch-3.4.list; sudo curl -o /etc/apt/sources.list.d/$FILE http://debian.tryton.org/debian/$FILE

*Distribution stretch-3.6*

::

 $ FILE=tryton-stretch-3.6.list; sudo curl -o /etc/apt/sources.list.d/$FILE http://debian.tryton.org/debian/$FILE

*Distribution stretch-3.8*

::

 $ FILE=tryton-stretch-3.8.list; sudo curl -o /etc/apt/sources.list.d/$FILE http://debian.tryton.org/debian/$FILE

*Distribution stretch-4.0*

::

 $ FILE=tryton-stretch-4.0.list; sudo curl -o /etc/apt/sources.list.d/$FILE http://debian.tryton.org/debian/$FILE

*Distribution stretch-4.2*

::

 $ FILE=tryton-stretch-4.2.list; sudo curl -o /etc/apt/sources.list.d/$FILE http://debian.tryton.org/debian/$FILE

*Distribution stretch-4.4*

::

 $ FILE=tryton-stretch-4.4.list; sudo curl -o /etc/apt/sources.list.d/$FILE http://debian.tryton.org/debian/$FILE

*Distribution stretch-4.6*

::

 $ FILE=tryton-stretch-4.6.list; sudo curl -o /etc/apt/sources.list.d/$FILE http://debian.tryton.org/debian/$FILE

 *Distribution stretch-5.0*

::

 $ FILE=tryton-stretch-5.0.list; sudo curl -o /etc/apt/sources.list.d/$FILE http://debian.tryton.org/debian/$FILE



*Distribution buster-3.4*

::

 $ FILE=tryton-buster-3.4.list; sudo curl -o /etc/apt/sources.list.d/$FILE http://debian.tryton.org/debian/$FILE

*Distribution buster-3.6*

::

 $ FILE=tryton-buster-3.6.list; sudo curl -o /etc/apt/sources.list.d/$FILE http://debian.tryton.org/debian/$FILE

*Distribution buster-3.8*

::

 $ FILE=tryton-buster-3.8.list; sudo curl -o /etc/apt/sources.list.d/$FILE http://debian.tryton.org/debian/$FILE

*Distribution buster-4.0*

::

 $ FILE=tryton-buster-4.0.list; sudo curl -o /etc/apt/sources.list.d/$FILE http://debian.tryton.org/debian/$FILE

*Distribution buster-4.2*

::

 $ FILE=tryton-buster-4.2.list; sudo curl -o /etc/apt/sources.list.d/$FILE http://debian.tryton.org/debian/$FILE

*Distribution buster-4.4*

::

 $ FILE=tryton-buster-4.4.list; sudo curl -o /etc/apt/sources.list.d/$FILE http://debian.tryton.org/debian/$FILE

*Distribution buster-4.6*

::

 $ FILE=tryton-buster-4.6.list; sudo curl -o /etc/apt/sources.list.d/$FILE http://debian.tryton.org/debian/$FILE

*Distribution buster-5.0*

::

 $ FILE=tryton-buster-5.0.list; sudo curl -o /etc/apt/sources.list.d/$FILE http://debian.tryton.org/debian/$FILE


Installation
============

Update package lists

::

 $ sudo apt-get update
 
 
 
Installing the server

::

 $ sudo apt-get install tryton-server [-t <distribution>]

 
Installing all modules incl. server

::

 $ sudo apt-get install tryton-modules-all [-t <distribution>]


Installing the client

::

 $ sudo apt-get install tryton-client [-t <distribution>]



Pinning
=======

The versioning of Tryton packages from debian.tryton.org is conservative with respect to upgrades. It follows
the ususal backports schema to provide upgradibility to a new Debian release at every moment. Therefore it may be, 
that apt will want to replace the installed version with a *newer* version number, that is available from other sources.lists.

To stick with the packages of debian.tryton.org, it is possible to pin the packages. With pin priority >1000 they will 
be preferred over other packages. A sample looks like

::

  Package: *
  Pin: release o=debian.tryton.org
  Pin-Priority: 999


Ready-to-use
------------

A suitable file can be downloaded with

::

 $ sudo curl -o /etc/apt/preferences.d/debian.tryton.org.pref http://debian.tryton.org/debian/debian.tryton.org.pref
 
 
Using the new preferences

::

 $ sudo apt-get update && apt-get dist-upgrade

