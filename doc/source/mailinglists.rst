.. include:: substitutions.txt

.. _Mailing Lists:

=============
Mailing Lists
=============

The communication about development, uploads, bug messages, vcs commits etc. is done via mailing lists.

To read the archives or to subscribe follow the links on the Overview Page.

Overview
========

  `Tryton Debian`_ (Conversation of all kind for Debian Tryton Maintainers)
  
  `Debian Package Tracker`_ (For messages on the Debian Repositories)



.. _Tryton Debian: http://lists.alioth.debian.net/cgi-bin/mailman/listinfo/tryton-debian
.. _Debian Package Tracker: https://tracker.debian.org/teams/debian-tryton/
