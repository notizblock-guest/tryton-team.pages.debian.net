.. include:: substitutions.txt

.. _Download:

========
Download
========

The download URLs are indicated on the summary of each package on salsa.debian.org.

Anonymous download
==================

Via https protcol

::

  $ git clone https://salsa.debian.org/tryton-team/<package>.git


Authenticated download (Developers with push access)
====================================================


With ssh key registered on the server

::

  $ git clone git@salsa.debian.org:tryton-team/cached-property.git




Download of the complete tree
=============================

To get a complete list of the group have a look at list_projects.sh in

https://salsa.debian.org/mehdi/salsa-scripts



