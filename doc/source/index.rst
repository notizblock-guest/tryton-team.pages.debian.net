.. include:: substitutions.txt

==========================
Tryton Packages for Debian
==========================

Apt Package Repository (Unofficial)
-----------------------------------

.. toctree::
   :maxdepth: 2

   mirror
   
   gnuhealth

Browse the apt mirror of debian.tryton.org:

   `Tryton Apt Repository`_

.. _Tryton Apt Repository: http://debian.tryton.org/debian/


Development
-----------

.. toctree::
   :maxdepth: 2

   download
   
   mailinglists

   links

Browse the git repositories on salsa.debian.org:

   `Git Repositories`_

.. _Git Repositories: https://salsa.debian.org/tryton-team 

Indices and tables
------------------

* :ref:`search`

