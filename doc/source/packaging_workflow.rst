.. include:: substitutions.txt

.. _Workflow:

==================
Packaging Workflow
==================

Voraussetzungen
===============

Wir arbeiten nach dem Workflow von git-stuff:

::

  $ apt-get install git-stuff (-t experimental , jedenfalls die neueste verfügbare Version)
  
Siehe dann die zum Paket gehörende Doku unter

/usr/share/doc/git-stuff/packaging-workflow-with-git-stuff.html

und installiere die unter 2) Preparations genannten Pakete


Neues Paket
===========

Kurzanleitung abweichend von o.g. Doku

* Den Tarball packen wir mittlerweile mit xz, d.h. für den tarball ist diese Kompression zu verwenden, 
  entsprechend ändert sich die Dateiendung.

::

  $ mv package-version.tar.xz package_version.orig.tar.xz

* Repos in gitolite-admin eintragen und klonen

::

  $ git clone git@192.168.111.105:<reposname>.git


* Tarball in dieses Repos entpacken

* Bei Python-Paketen ist die eigentliche Quellebene unterhalb der ersten Ebene, deshalb
  
::

  $ mv <package-name>/* .
  $ rmdir <package-name>

* Upstream-Branch 
  
::

  $ git-upstream-add version

 
* Pristine-tar Branch
  
::

  $ git-upstream-tar ../<package-name>.orig.tar.xz

* Debian Branch
  
::

  $ git checkout -b debian

* debian dir erstellen bzw. reinkopieren
* debian dir anpassen
* Paket im chroot packen
* Wenn lintian clean und erfolgreiche Probeinstallation
  
::

  $ git add .
  $ git commit -a --amend -m "Adding debian version <version>-1."
  $ git-debian-tag <version>-1

* Änderungen auf den Server pushen

::

  $ git push --all
  $ git push --tags

 



Basic Workflow
==============

#. wget http://downloads.tryton.org/2.1/trytond-2.1.1.tar.gz
#. cd tryton-server
#. mv ../trytond-2.1.1.tar.gz ../tryton-server_2.1.1.orig.tar.gz
#. git checkout upstream
#. rm -rf *
#. tar xfz ../tryton-server_2.1.1.orig.tar.gz
#. mv trytond-2.1.1/* ./
#. rmdir trytond-2.1.1
#. git-upstream-add 2.1.1
#. git-upstream-tar ../tryton-server_2.1.1.orig.tar.gz
#. git checkout debian
#. git cherry-pick -n upstream
#. git commit -a -m "Merging upstream version 2.1.1."
#.  -> hier evtl. eigene Anpassungen einpflegen und committen
#. git-debian-changelog (Version ggf. manuell anpassen bei neuem Release)
#. git-debian-release
#. git-push (sic!, ansonsten git push + git push --tags)


