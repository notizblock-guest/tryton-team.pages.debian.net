.. include:: substitutions.txt

.. _Layout:

======
Layout
======


Branch Layout Debian-Repos
==========================

Typischer Branch-Aufbau eines Git-Repos für Tryton in Debian

::

  * debian
    debian-squeeze
    debian-squeeze-1.6
    debian-squeeze-1.8
    debian-squeeze-1.8vth
    debian-squeeze-2.2
    debian-squeeze-2.2vth
    debian-wheezy-1.8vth
    debian-wheezy-2.2vth
    pristine-tar
    pristine-tar-vth
    upstream
    upstream-1.6
    upstream-1.8
    upstream-1.8vth
    upstream-2.2vth


Upstream
--------

Elementar sind die Branches debian, upstream, pristine-tar (s. Basic Workflow)

Für Backports wird per Debian Release der debian Branch geforkt.

Um die verschiedenen Tryton-Versionen auf den Debian Releases abbilden zu können, werden 
zusätzlich pro Debian-Release Sub-Branches benötigt, die nach den Tryton-Versionen benannt
werden (Branch upstream muss nun mitgeforkt werden).

Intern
------

Für den internen Gebrauch werden die Sub-Branches weiter geforkt und mit dem Zusatz vth
versehen (ab hier muss zusätzlich pristine-tar geforkt werden, da die internen Versionen
nicht öffentlich erscheinen sollen). Diese Branches werden nur intern gepusht.


