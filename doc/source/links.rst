.. include:: substitutions.txt

.. _Links:

=====
Links
=====

Intro
=====

git - the simple guide http://rogerdudler.github.com/git-guide/


Cheat pages
===========

Git Stuff - Debian Packaging Workflow with Git http://packages.debian.org/sid/git-stuff

Git - Version Control System (Daniel) http://wiki.progress-linux.org/software/git/

$cheat git http://cheat.errtheblog.com/s/git


Special
=======

Managing remotes http://help.github.com/remotes/ http://stackoverflow.com/questions/849308/pull-push-from-multiple-remote-locations

Distributed Workflows: What to do when a push fails http://book.git-scm.com/3_distributed_workflows.html

Pick individual commits http://gitready.com/intermediate/2009/03/04/pick-out-individual-commits.html

Merge specific commit http://stackoverflow.com/questions/881092/how-to-merge-a-specific-commit-in-git

