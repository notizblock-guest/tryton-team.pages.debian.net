.. include:: substitutions.txt

.. _GNU Health Debian Apt Repository:

================================
GNU Health Debian Apt Repository
================================

Introduction
============

`GNU Health`_ is a Free Health and Hospital Information System that provides the following functionalities:

  * Electronic Medical Record (EMR)
  * Hospital Information System (HIS)
  * Health Information System

This site provides distribution packages by `Debian Tryton Maintainers`_, that are available
for Debian stable, testing and unstable. 

With the intent to provide a common installation method the `GNU Health Project`_ provides
an installation method using a bash script (see also the `GNU Health Installation Guide`_). Since this script
aims to be distribution agnostic, it uses methods that do not integrate well with best practices
on Debian systems.

A short listing of the advantages when using proper Debian packages:

  * Full automatic handling of package dependencies.
  * Full integration with apt, the distribution package manager (e.g. automatic update of the GNU Health installation together with system updates).
  * Security support for distribution packages for the lifetime of the distribution.
  * Integration of the daemon start and maintenance (e.g. configuration and start of the daemon provided by the integrated init system).
  * Management of the server with distribution tools.

Quick Start
===========

For the quick and impatient there exists  a `Docker Setup for GNU Health Demoserver`_, that allows one to get up and running easily
a ready-to-use GNU Health Demo server based on Debian packages (see below).


What is different from Tryton core modules?
===========================================

Only the numbering schema. GNU Health modules are technically just another set of Tryton modules. The 
only difference to bear in mind is that the `GNU Health Project`_ uses a different
numbering schema compared to the targeted `Tryton`_ series. So you have to gather just
the correct Tryton series to match the GNU Health modules you want to use.

.. _Tryton: http://www.tryton.org/
.. _GNU Health Project: http://health.gnu.org/
.. _GNU Health: https://en.wikipedia.org/wiki/GNU_Health
.. _GNU Health Installation Guide: https://en.wikibooks.org/wiki/GNU_Health/Installation
.. _Debian Tryton Maintainers: http://debian.tryton.org 

Tryton series to GNU Health series
==================================

The pattern is following the schema

================= =============
GNU Health Series Tryton Series
================= =============
2.6               3.2
2.8               3.4
3.0               3.8
================= =============

Example:

If you want to install GNU Health 3.0, use the Tryton series 3.8.

Just proceed to install the sources.list for Tryton 3.8 as described under :doc:`mirror` and you will automatically
get the appropriate GNU Health modules.

For the server configuration please refer as usual to /usr/share/doc/tryton-server/README.Debian on your 
Debian system or look at the `latest version of README.Debian in the VCS`_.

.. _latest version of README.Debian in the VCS: https://alioth.debian.org/plugins/scmgit/cgi-bin/gitweb.cgi?p=tryton/tryton-server.git;a=blob_plain;f=debian/tryton-server.README.Debian;hb=HEAD

.. note:: Packaging by Debian Tryton Maintainers has started from
        GNU Health series 2.8.


The Health modules
------------------

GNU Health modules are individually packaged and installable in the style of all
Tryton modules in Debian to provide the modularity of the framework on installation level.

For ease of installation there are special modules available providing common setups:

For installing the GNU Health base profile (incl. server)

::

 # apt-get install tryton-modules-health-profile


For installing all GNU Health modules at once (incl. server, e.g. for testing)

::

 # apt-get install tryton-modules-health-all



Demo Database
=============

To ease the initial contact with GNU Health we have created an easy to use docker setup
based on packages from debian.tryton.org. This docker based setup provides a ready-to-use
GNU Health Demoserver using the same database that is used in the `GNU Health Online Demo Database`_.

.. _GNU Health Online Demo Database: https://en.wikibooks.org/wiki/GNU_Health/The_Demo_database#The_online_Demo_Database

Instructions for the usage of this docker setup can be found at

 `Docker Setup for GNU Health Demoserver`_

 `Run GNU Health from Docker (Lightweight Containers)`_


.. _Docker Setup for GNU Health Demoserver: https://github.com/mbehrle/docker-gnuhealth-demo
.. _Run GNU Health from Docker (Lightweight Containers): https://en.wikibooks.org/wiki/GNU_Health/Different_ways_to_test_GNU_Health#Option_4:_Run_GNU_Health_from_Docker_.28Lightweight_Containers.29
 
 
Debian Blends
=============

GNU Health packages are part of `Debian Med Blends`_.

.. _Debian Med Blends: http://blends.debian.org/med/tasks/his#tryton-modules-health


